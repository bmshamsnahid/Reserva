$( document ).ready(function() {
    const token = localStorage.getItem('token');
    if (token) {
        axios.get('/api/authentication/me', {
            headers: {
                "x-auth-token": token
            }
        }).then((response) => {
                if (response.data.success) {
                    const userInfo = response.data.data;
                    const userNameField = $('#userNameField');
                    userNameField.html(userInfo.name);
                } else {

                }
            })
            .catch(function (error) {
                console.log(error);
            });
    } else {
        $(location).attr("href", "/view/join");
    }
});
