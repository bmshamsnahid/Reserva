$( document ).ready(function() {
    console.log('This is update category.');
    const updateCategoryButton = $('#updateCategoryButton');
    updateCategoryButton.click(() => {
        const id = $('#categoryId').text();
        
        const token = localStorage.getItem('token');
        if (!token) return $(location).attr("href", "/view/join");

        const newCategoryName = $('#UpdateCategoryName').val();

        const body = {
            name: newCategoryName,
        };
  
        const options = {
            headers: {
                'Content-Type': 'application/json',
                "x-auth-token": token
            }
        };

        axios.patch('/api/category/' + id, body, options)
        .then(function (response) {
          if (response.data.success) {
              console.log(response.data.data);
              toastr.success('Category name updated.', 'Success');
          }
        })
        .catch(function (error) {
            console.log(error.response);
            toastr.error(error.response.data.message, 'Error');
        });
    });
});
