$( document ).ready(function() {
    const createAdminButton = $('#createAdminButton');

    createAdminButton.click(() => {
        const createAdminName = $('#createAdminName').val();
        const createAdminEmail = $('#createAdminEmail').val();
        const createAdminPassword = $('#createAdminPassword').val();
        const createAdminPhone = $('#createAdminPhone').val();

        const token = localStorage.getItem('token');
        if (!token) return $(location).attr("href", "/view/join");

        const body = {
            name: createAdminName,
            email: createAdminEmail,
            phone: createAdminPhone,
            password: createAdminPassword,
            authenticationType: "local",
            isAdmin: true,
        };
        const options = {
            headers: {
                'Content-Type': 'application/json',
                "x-auth-token": token
            }
        };

        axios.post('/api/userInfo', body, options)
          .then(function (response) {
            if (response.data.success) {
                toastr.success('Admin created successfully.', 'Success')
            }
          })
          .catch(function (error) {
            console.log(error.response.data.message);
            toastr.error(error.response.data.message, 'Error')
          });
    });
});
