const mongoose = require('mongoose');
const Joi = require('joi');
const dataTables = require('mongoose-datatables');

const categorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        minLength: 1,
        maxLength: 30,
        unique: true,
    }
});

const validateCategory = (category) => {
    const schema = {
        name: Joi.string().min(1).max(30).required()
    };
    return Joi.validate(category, schema);
};

categorySchema.plugin(dataTables);

const Category = mongoose.model('category', categorySchema);

exports.categorySchema = categorySchema;
exports.validateCategory = validateCategory;
exports.Category = Category;
