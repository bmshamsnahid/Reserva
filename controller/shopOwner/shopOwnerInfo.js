const { UserInfo } = require('../../model/user/userInfo');

const getShopOwnerDataTable = async (req, res, next) => {

    UserInfo.dataTables({
        limit: req.body.length,
        skip: req.body.start,
        find: { isOwner: true },
        search: {
            value: req.body.search.value,
            fields: ['name', 'email']
          },
        order: req.body.order,
        columns: req.body.columns
      }).then(function (table) {
        res.json({
          data: table.data,
          recordsFiltered: table.total,
          recordsTotal: table.total
        });
      });
};

const changeShopOwnerApproval = async (req, res, next) => {
    const shopOwner = await UserInfo.findById(req.params.id);
    shopOwner.isApproved = !shopOwner.isApproved;
    const result = await shopOwner.save();
    return res.status(200).json({ success: true, data: result });
};

module.exports = {
    getShopOwnerDataTable,
    changeShopOwnerApproval,
};
