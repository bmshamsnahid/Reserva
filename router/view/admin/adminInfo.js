const express = require('express');
const router = express.Router();
const { UserInfo } = require('../../../model/user/userInfo');

router.get('/createAdminDisplay', (req, res, next) => {
    return res.render('admin/createAdmin');
});

router.get('/updateAdminDisplay/:id', async (req, res, next) => {
    // console.log('Id: ' + req.params.id);
    const admin = await UserInfo.findById(req.params.id).select('-password -__v');
    console.log(admin);
    res.render('admin/updateAdmin', { admin: admin });
});

router.get('/adminListDisplay', (req, res, next) => {
    return res.render('admin/adminList');
});

module.exports = router;
