const express = require('express');
const router = express.Router();

router.get('/createProductDisplay', (req, res, next) => {
    return res.render('product/createProduct');
});

router.get('/updateProductDisplay', (req, res, next) => {
    return res.render('product/updateProduct');
});

router.get('/productListDisplay', (req, res, next) => {
    return res.render('product/productList');
});

module.exports = router;
