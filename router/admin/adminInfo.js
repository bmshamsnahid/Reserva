const express = require('express');
const router = express.Router();
const adminController = require('../../controller/admin/adminInfo');

router.get('/', adminController.getAllAdmin);
router.get('/:skip/:limit', adminController.getAdminPagination);
router.get('/count', adminController.adminCount);
router.post('/get-admin-data', adminController.getAllAdminData);

module.exports = router;
